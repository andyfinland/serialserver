#include "httpd.h"
#include "../z80serial/z80serial.h"
#include "utils.h"
#include "command.h"


int main(int c, char** v)
{
    serve_forever("8080");
    return 0;
}

void route()
{
    ROUTE_START()

    ROUTE_GET("/")
    {
        headerOK();
        printf("Serial Server: \r\n%s", request_header("User-Agent"));
    }

    ROUTE_GET("/dump")
    {
        dumpCommand();
    }

    ROUTE_GET("/run")
    {
        runCommand();
    }

    ROUTE_GET("/reset")
    {
        runReset();
    }

    ROUTE_GET("/ready")
    {
        runReady();
    }

    ROUTE_GET("/dir")
    {
        runDir();
    }

    ROUTE_POST("/")
    {
        headerOK();

        printf("POST payload size: %d bytes. \r\n", payload_size);
        printf("Fetch the data using `payload` variable.");
    }
  
    ROUTE_END()
}




