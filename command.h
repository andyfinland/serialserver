

void sendCommand(char * command, char * device, char * address);
void dumpCommand();
void runCommand();
void runReady();
void runDir();
void runReset();
void singleCommand(char * command);
void paramsCommand(char * command);
void headerOK();
void headerBad();

/* DIR */
void runDir() {
    singleCommand("dir");
}

/* READY */
void runReady() {
    singleCommand("ready");
}

/* RESET */
void runReset() {
    singleCommand("reset");
}

/* RUN */
void runCommand() {
    paramsCommand("run ");
}

/* DUMP */
void dumpCommand() {
    paramsCommand("dump ");
}


/* Single command */
void singleCommand(char * command) {

    int count;
    char **tokens;

    /* split uri */
    count = split (qs, '&', &tokens);
    char * devicePath = tokens[0];
    count = split (devicePath, '=', &tokens);
    char * device = tokens[1];

    /* reset */
    sendCommand(command, concat("/dev/",device), "");

    /* freeing tokens */
    for (int i = 0; i < count; i++) free (tokens[i]);
    free (tokens);

}

/* Send command params */
void paramsCommand(char * command) {

    int count;
    char **tokens;

    /* split uri */
    count = split (qs, '&', &tokens);

    if (count == 1) {

        headerBad();
        printf("Error: Missing params\r\n");

    } else {

        char * addressPath = tokens[1];
        char * devicePath = tokens[0];

        count = split (devicePath, '=', &tokens);
        char * device = tokens[1];

        count = split (addressPath, '=', &tokens);
        char * address = tokens[1];

        /* dump */
        sendCommand(command, concat("/dev/",device), address);

    }

    /* freeing tokens */
    for (int i = 0; i < count; i++) free (tokens[i]);
    free (tokens);

}


/* Send command */
void sendCommand(char * command, char * device, char * address) {

    int port = openPort(device);

    if (port>0) {

        headerOK();

        // config port 115200
        configPort(port);
        
        // send command
        writePort(port, concat(command,address));

        // print output
        char * str = readPort(port);
        printf("%s",str);

        // close port
        closePort(port);

    } else {

        headerBad();
        printf("Error: Failed to open device\r\n");

    }
}

void headerOK() {

    printf("HTTP/1.1 200 OK\r\n");
    printf("Content-Type: text/plain\r\n");
    printf("Pragma: no-cache\r\n");
    printf("Cache-Control: no-cache\r\n");
    printf("Connection: close\r\n");
    printf("\r\n");
}

void headerBad() {

    printf("HTTP/1.1 404 Not Found\r\n");
    printf("\r\n");
}